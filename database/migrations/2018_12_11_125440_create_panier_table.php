<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanierTable extends Migration {

	public function up()
    {
        Schema::create('panier', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('prod_id')->unsigned();
            $table->integer('user_id')->unsigned();

        });
        Schema::table('panier', function (Blueprint $table) {
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

	public function down()
	{
		Schema::drop('panier');
	}
}
