<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImgTable extends Migration {

	public function up()
    {
        Schema::create('img', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('prod_id')->unsigned()->default(Null);
            $table->integer('actu_id')->unsigned()->default(Null);

        });
        Schema::table('img', function (Blueprint $table) {
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('actu_id')->references('id')->on('actus')->onDelete('cascade');
        });
    }

	public function down()
	{
		Schema::drop('img');
	}
}
