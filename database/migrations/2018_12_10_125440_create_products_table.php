<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->timestamps();
			$table->string('title', 255);
			$table->text('description');
			$table->text('linkImg')->nullable();
			$table->text('categorie')->nullable();
			$table->text('color')->nullable();
			$table->float('price')->default(0);

        });
	}

	public function down()
	{
		Schema::drop('products');
	}
}
