<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActusTable extends Migration {

	public function up()
	{
		Schema::create('actus', function(Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->timestamps();
			$table->string('title', 255);
			$table->text('content');
			$table->string('linkImg');
		});
	}

	public function down()
	{
		Schema::drop('actus');
	}
}
