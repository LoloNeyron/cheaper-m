<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\products
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\products newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\products newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\products query()
 * @mixin \Eloquent
 */
class products extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'linkImg', 'categorie', 'color', 'price'];

    /**
     * Attributes for timestamps
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];



}
