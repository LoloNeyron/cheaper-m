<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\actus;


class ActusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'=>'required',
            'content'=> 'required',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        if(request()->img !== null){
            $imageName = time().'.'.request()->img->getClientOriginalExtension();
            request()->img->move(public_path('images/ActusImg'), $imageName);
            $pathImg = url('/images/ActusImg/' . $imageName);
            $actu = new actus([
                'title' => $request->get('title'),
                'content'=> $request->get('content'),
                'linkImg'=> $pathImg
            ]);
        }else{
            $actu = new actus([
                'title' => $request->get('title'),
                'content'=> $request->get('content'),
                'linkImg'=> $request->get('img')
            ]);
        }

        $actu->save();
        return redirect('/')->with('success', 'Ajout de la nouvelle actualité !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
