<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function HomeView() {
        $actusTheLast = DB::table('actus')->get();
        $productTheLast = DB::table('products')->get();
        //dd($actusTheLast);
        if (Auth::check()){
            $userIsAdmin = Auth::user()->admin;
            return view('welcome', compact('userIsAdmin', 'actusTheLast', 'productTheLast'));
        }else{
            $userIsAdmin = 0;
            return view('welcome', compact('userIsAdmin', 'actusTheLast', 'productTheLast'));
        }
    }
}
