<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'=>'required',
            'description'=> 'required',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'price' => 'required'
        ]);


        if(request()->img !== null){
            $imageName = time().'.'.request()->img->getClientOriginalExtension();
            request()->img->move(public_path('images/ProductImg'), $imageName);
            $pathImg = url('/images/ProductImg/' . $imageName);
            $product = new products([
                'title' => $request->get('title'),
                'description'=> $request->get('description'),
                'linkImg'=> $pathImg,
                'categorie' => $request->get('categorie'),
                'color' => $request->get('color'),
                'price' => $request->get('price')
            ]);
        }else{
            $product = new products([
                'title' => $request->get('title'),
                'description'=> $request->get('description'),
                'linkImg'=> $request->get('img'),
                'categorie' => $request->get('categorie'),
                'color' => $request->get('color'),
                'price' => $request->get('price')
            ]);
        }
        $product->save();
        return redirect('/')->with('success', 'le produit a bien été ajoutée.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
