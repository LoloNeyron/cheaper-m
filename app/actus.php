<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\actus
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\actus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\actus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\actus query()
 * @mixin \Eloquent
 */
class actus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actus';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'linkImg'];

    /**
     * Attributes for timestamps
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

}
