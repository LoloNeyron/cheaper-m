describe('Premier Test', function() {
    it('verifie la connexion', function() {
        cy.visit('http://127.0.0.1:8000/');
        cy.contains('Laravel');

        cy.contains('Login').click();
        cy.contains('E-Mail Address');
        cy.contains("Password");

        cy.get("#email")
            .type("test@test.com");

        cy.get("#password")
            .type("test123");

        cy.get('[type="submit"]').click();

        cy.contains("test");
    })
});
