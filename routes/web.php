<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@HomeView')->name('Welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('StoreActus', 'ActusController@store')->name('StoreActus');

Route::post('StoreProducts', 'ProductsController@store')->name('StoreProducts');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminController@indexAdmin')->name('AdminIndex');


