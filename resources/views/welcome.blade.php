@extends('layouts.app')
@section('title')
    Actualité
@endsection

@section("DropDownMenuTopBar")
    @if ($userIsAdmin === 1)
        <a class="dropdown-item" style='cursor: pointer;' data-toggle="modal" data-target="#ModalActus">
            Du nouveau ?
        </a>
        <a class="dropdown-item" style='cursor: pointer;' data-toggle="modal" data-target="#ModalProduct">
            Ajouter un nouveau produit.
        </a>
    @else
        <h6>Tu n'as pas les droits requis pour ajouter du contenue.</h6>
    @endif
@endsection

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2">
                <!-- Side Bar Gauche Products-->
                @if($productTheLast !== NULL)
                    @foreach($productTheLast as $product)
                        <div class="hoverableCard" type="button" data-toggle="modal" data-target="#pro{{ $product->id }}" >
                            @if($product->linkImg !== NULL)
                                {{--TODO si plusieur image -> logo indique plusieur images--}}
                                <img class="card-img-top"  alt="Card image cap" src="{{$product->linkImg}}">
                            @endif
                            <div class="card" type="button" data-toggle="modal" data-target="#pro{{ $product->id }}">
                                <div class="card-body">
                                    <h4 class="card-title">{{$product->title}}</h4>
                                    <p class="card-text">{{$product->description}}</p>
                                    <p class="text-muted right">{{ Carbon\Carbon::parse($product->created_at)->format('d M Y') }}</p>
                                </div>
                            </div>
                        </div>
                        <br>

                    @endforeach
                @else
                    <h3>No Products</h3>
                @endif
            </div>
            <div class="col-lg-8">

            </div>
            <div class="col-lg-2">
                <!--Side Bar Droite actus-->
                @if($actusTheLast !== 0)
                    @foreach($actusTheLast as $actu)
                        @if($actu->linkImg !== NULL)
                            <img type="button" data-toggle="modal" data-target="#act{{ $actu->id }} "class="hoverable card-img-top" src="{{$actu->linkImg}}" alt="Card image cap">
                        @endif
                        <div type="button" data-toggle="modal" class="hoverable" data-target="#act{{ $actu->id }}" class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$actu->title}}</h4>
                                <p class="card-text">{{$actu->content}}</p>
                                <p class="text-muted right">{{ Carbon\Carbon::parse($actu->created_at)->format('d M Y') }}</p>
                            </div>
                        </div>
                            <br>
                    @endforeach
                @else
                    <h3>No news</h3>
                @endif
            </div>
        </div>
    </div>

    <!-- modal gestion de contenu -->
<div class="modal fade" id="ModalActus" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Quoi de neuf ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action={{ route('StoreActus') }} enctype="multipart/form-data" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputTitle">Titre</label>
                        <input name="title" type="text" class="form-control" id="inputTitle" aria-describedby="titleHelp" placeholder="Entrer un titre">
                        <small id="titleHelp" class="form-text text-muted">Le titre de votre actualité.</small>
                    </div>
                    <div class="form-group">
                        <label for="inputContent">Contenu</label>
                        <textarea name="content" class="form-control" placeholder="Dite nous tout..." id="formControlTextarea" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="formControlFile1">Des photos</label>
                        <input name="img" multiple type="file" class="form-control-file" id="formControlFile1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Publier</button>
                </div>
            </form>
        </div>
    </div>
</div>

    <!-- modal gestion de produits -->
    <div class="modal fade" id="ModalProduct" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nouveau Produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action={{ route('StoreProducts') }} enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputTitle">Nom du produit</label>
                            <input name="title" type="text" class="form-control" id="inputTitle" aria-describedby="titleHelp" placeholder="Entrer un titre">
                        </div>
                        <div class="form-group">
                            <label for="inputContent">Description de votre produit.</label>
                            <textarea name="description" class="form-control" placeholder="Dite nous tout sur ce produit ..." id="formControlTextarea" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="InputColor">Couleur du produit</label>
                            <input name="color" type="text" class="form-control" id="inputColor">
                        </div>
                        <div class="form-group">
                            <label for="InputCategorie">Catégorie</label>
                            <input name="categorie" type="text" class="form-control" id="inputCategorie" placeholder="Porte, fenêtre, ...">
                        </div>
                        <div class="form-group">
                            <label for="inputPrice">Prix HT:</label>
                            <input name="price" type="number" step="0.001" class="form-control" placeholder="... €" id="inputPrice"></input>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="formControlFile1">Des photos de votre produit</label>
                            <input name="img" multiple type="file" class="form-control-file" id="formControlFile1">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Ajouter ce produits</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @foreach($productTheLast as $product)
        <div class="modal" id="pro{{$product->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$product->title}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" container>
                        <div class="row">
                            <div class="col-sm-12">
                                <img width="100%" src="{{ $product->linkImg }}" alt="{{ $product->title }}">
                                {{--TODO Si plusieur image -> Carrousel--}}
                                <legend>Description :</legend>
                                <fieldset class="alert alert-dark">
                                    <p>{{$product->description}}</p>
                                </fieldset>
                                <blockquote>{{$product->categorie}}</blockquote>
                                <blockquote>{{$product->color}}</blockquote>
                                @if(Auth::check())
                                    @if(Auth::user()->type === "autre" )
                                        <p>{{$product->price}}€ H.T</p></div>
                                    @endif
                                @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        {{--TODO faire le lien pour le button "Faire une simulation de devis"--}}
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Faire une simulation de devis</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @foreach($actusTheLast as $actu)
        <div class="modal" id="act{{$actu->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$actu->title}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" container>
                        <div class="row">
                            <div class="col-sm-12">
                                <img width="100%" src="{{ $actu->linkImg }}" alt="{{ $actu->title }}">
                                {{--TODO Si plusieur image -> Carrousel--}}
                                <legend>Description :</legend>
                                <fieldset class="alert alert-dark">
                                    <p>{{$actu->content}}</p>
                                </fieldset>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    @endforeach
@endsection

@section('script')
    <script>

    </script>
@endsection
